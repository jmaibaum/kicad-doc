:experimental:

== Footprints and footprint libraries

[[managing-footprint-libraries]]
=== Managing footprint libraries

KiCad's footprint library management system allows directly using several types
of footprint libraries:

* KiCad `.pretty` footprint libraries
  (folders with .pretty extension, containing .kicad_mod files)
* KiCad Legacy footprint libraries (.mod files)
* GEDA libraries  (folders containing .fp files)
* Eagle footprint libraries

NOTE: KiCad only supports writing to KiCad's native `.pretty` format footprint
libraries (and the `.kicad_mod` footprint files within them). All other
footprint library formats are read-only.

KiCad uses a table of footprint libraries to map footprint libraries of any
supported library type to a library nickname. KiCad uses a global footprint
library table as well as a table specific to each project. To edit either
footprint library table, use **Preferences** -> **Manage Footprint
Libraries...**.

image::images/en/fp_lib_table.png[scaledwidth="80%",alt="footprint library table dialog"]

The global footprint library table contains the list of libraries that are
always available regardless of the currently loaded project. The table is
saved in the file `fp-lib-table` in the KiCad configuration folder.
xref:../kicad/kicad.adoc#config-file-location[The location of this folder]
depends on the operating system being used.

The project specific footprint library table contains the list of libraries that
are available specifically for the currently loaded project. If there are any
project-specific footprint libraries, the table is saved in the file
`fp-lib-table` in the project folder.

==== Initial Configuration

The first time the PCB Editor (or any other KiCad tool that uses footprints) runs
and the global footprint table file `fp-lib-table` is not found, KiCad will
guide the user through setting up a new footprint library table. This process is
described <<initial-configuration,above>>.

==== Managing Table Entries

Footprint libraries can only be used if they have been added to either the
global or project-specific footprint library table.

Add a library either by clicking the
image:images/icons/small_folder_16.png[Folder icon] button and selecting a
library or clicking the image:images/icons/small_plus_16.png[Plus icon] button
and typing the path to a library file. The selected library will be added to the
currently opened library table (Global or Project Specific). Libraries can be
removed by selecting desired library entries and clicking the
image:images/icons/small_trash_16.png[Delete icon] button.

The image:images/icons/small_up_16.png[Up icon] and
image:images/icons/small_down_16.png[Down icon] buttons move the selected
library up and down in the library table. This does not affect the display order
of libraries in the Footprint Library Browser, Footprint Editor, or Add
Footprint tool.

Libraries can be made inactive by unchecking the *Active* checkbox in the first
column. Inactive libraries are still in the library table but do not appear in
any library browsers and are not loaded from disk, which can reduce loading
times.

A range of libraries can be selected by clicking the first library in the range
and then kbd:[Shift]-clicking the last library in the range.

Each library must have a unique nickname: duplicate library nicknames are not
allowed in the same table. However, nicknames can be duplicated between the
global and project library tables. Libraries in the project table take
precedence over libraries with the same name in the global table.

Library nicknames do not have to be related to the library filename or path. The
colon character (`:`) cannot be used in library nicknames or footprint names
because it is used as a separator between nicknames and footprints.

Each library entry must have a valid path. Paths can be defined as absolute,
relative, or by <<environment-variable-substitution,environment variable substitution>>.

The appropriate library format must be selected in order for the library to be
properly read. KiCad supports reading KiCad (`.pretty`), KiCad legacy (`.mod`),
Eagle (`.lbr`), and GEDA (folder with `.fp` files) footprint libraries.

There is an optional description field to add a description of the library
entry. The option field is not used at this time so adding options will
have no effect when loading libraries.

[[environment-variable-substitution]]
==== Environment Variable Substitution

The footprint library tables support environment variable substitution, which
allows you to define environment variables containing custom paths to where your
libraries are stored. Environment variable substitution is supported by using
the syntax `${ENV_VAR_NAME}` in the footprint library path.

By default, KiCad defines several environment variables:

* `${KIPROJMOD}` points to the current project directory and cannot be modified.
* `${KICAD6_FOOTPRINT_DIR}` points to the default location of KiCad's standard
  footprint libraries.
* `${KICAD6_SYMBOL_DIR}` points to the default location of KiCad's standard
  symbol libraries.
* `${KICAD6_3DMODEL_DIR}` points to the default location of KiCad's standard 3D
  model libraries.
* `${KICAD6_TEMPLATE_DIR}` points to the default location of KiCad's standard
  template library.

`${KIPROJMOD}` cannot be redefined, but the other environment variables can be
redefined and new environment variables added in the **Preferences** ->
**Configure Paths...** dialog.

Using environment variables in the footprint library tables allows libraries to
be relocated without breaking the footprint library tables, so long as the
environment variables are updated when the library location changes.

`${KIPROJMOD}` allows libraries to be stored in the project folder without
having to use an absolute path in the project library table. This makes it
possible to relocate projects without breaking their project library tables.

==== Using the GitHub Plugin

NOTE: KiCad removed support for the GitHub library plugin in version 6.0.

[[creating-and-editing-footprints]]
=== Creating and editing footprints

NOTE: TODO: Write this section

[[custom-pad-shapes]]
==== Custom pad shapes

[[footprint-attributes]]
==== Footprint attributes

NOTE: Mention net ties here

[[footprint-wizards]]
==== Footprint wizards

For more information about creating new footprint wizards, see the Scripting section of the
Advanced Topics chapter.
